var gulp = require('gulp'),
    less = require('gulp-less'),
    concatCss = require('gulp-concat-css'),
    minifyJs = require('gulp-uglify'),
    concatJs = require('gulp-concat'),
    clean = require('gulp-clean'),
    imagemin = require('gulp-imagemin');

/*
 *  GULP TASK JQUERY-BOOTSTRAP
 */
gulp.task('vendorJs', function () {
    return gulp.src([
        'bower_components/jquery/dist/jquery.js',
        'bower_components/bootstrap/dist/js/bootstrap.js'
    ])
        //.pipe(concatJs('bootstrap.js'))
        .pipe(minifyJs())
        .pipe(gulp.dest('public/assets/vendor/js/'));
});

gulp.task('vendorLess', function () {
   return gulp.src('bower_components/bootstrap/less/bootstrap.less')
       .pipe(less({
           compress: true
       }))
       .pipe(gulp.dest('public/assets/vendor/css/'))
});

gulp.task('vendorFont', function () {
   return gulp.src('bower_components/bootstrap/fonts/*')
       .pipe(gulp.dest('public/assets/vendor/fonts/'))
});

gulp.task('vendorClean', function () {
   return gulp.src([
       'public/assets/vendor/css/*',
       'public/assets/vendor/fonts/*',
       'public/assets/vendor/js/*'
   ])
       .pipe(clean());
});

gulp.task('vendor', ['vendorClean'], function () {
   var vendorTasks = ['vendorLess', 'vendorFont', 'vendorJs'];

    vendorTasks.forEach(function (task) {
      gulp.start(task);
   });
});

/*
 *  GULP TASK DEFAULT
 */
gulp.task('default', function () {
    var tasks = ['vendor'];

    tasks.forEach(function (task) {
        gulp.start(task);
    })
})
