<?php

namespace Src\DefaultModule\Dao;

use Src\DefaultModule\Repository\UserRepository;

/**
 * Class UserDao
 */
class UserDao
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * UserDao constructor.
     *
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Find one by id.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function findOneById($id)
    {
        //return $this->repository->findOne($id);
        return $this->repository->findOneBy(['id' => 1]);
    }
}
