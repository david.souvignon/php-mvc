<?php

namespace Src\DefaultModule\Model\Order;

/**
 * Class InProgressOrderStatus
 */
class InProgressOrderStatus extends AbstractOrderStatus
{
    /**
     * {@inheritdoc}
     */
    public function setOrderStatus(OrderModel $orderModel)
    {
        if (OrderModel::STATUS_PENDING === $orderModel->getStatus()) {
            $orderModel->setStatus(OrderModel::STATUS_PROGRESS);

            return $orderModel;
        }

        return $this->nextOrderStatus->setOrderStatus($orderModel);
    }
}
