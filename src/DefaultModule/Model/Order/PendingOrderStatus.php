<?php

namespace Src\DefaultModule\Model\Order;

/**
 * Class PendingOrderStatus
 */
class PendingOrderStatus extends AbstractOrderStatus
{
    /**
     * {@inheritdoc}
     */
    public function setOrderStatus(OrderModel $orderModel)
    {
        if (null === $orderModel->getStatus()) {
            $orderModel->setStatus(OrderModel::STATUS_PENDING);

            return $orderModel;
        }

        return $this->nextOrderStatus->setOrderStatus($orderModel);
    }
}
