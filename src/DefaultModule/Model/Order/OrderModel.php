<?php

namespace Src\DefaultModule\Model\Order;

use Core\Model\CoreModel;

/**
 * Class OrderModel
 */
class OrderModel extends CoreModel
{
    const STATUS_PENDING = 0;
    const STATUS_PROGRESS = 1;
    const STATUS_DELIVERED = 2;

    /**
     * @var int
     */
    protected $status;

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * OrderModel constructor.
     */
    public function __construct()
    {
        $this->status = null;
    }
}
