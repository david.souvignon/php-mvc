<?php

namespace Src\DefaultModule\Model\Order;

/**
 * Class DeliveredOrderStatus
 */
class DeliveredOrderStatus extends AbstractOrderStatus
{
    /**
     * {@inheritdoc}
     */
    public function setOrderStatus(OrderModel $orderModel)
    {
        if (OrderModel::STATUS_PROGRESS === $orderModel->getStatus()) {
            $orderModel->setStatus(OrderModel::STATUS_DELIVERED);
        }

        return $orderModel;
    }
}
