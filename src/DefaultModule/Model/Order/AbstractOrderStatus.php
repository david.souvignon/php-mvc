<?php

namespace Src\DefaultModule\Model\Order;

/**
 * Class AbstractOrderStatus
 */
abstract class AbstractOrderStatus
{
    /**
     * @var AbstractOrderStatus
     */
    protected $nextOrderStatus;

    /**
     * Set next status.
     *
     * @param AbstractOrderStatus $status
     */
    public function setNextStatus(AbstractOrderStatus $status)
    {
        $this->nextOrderStatus = $status;
    }

    /**
     * Set Order status.
     *
     * @param OrderModel $orderModel
     *
     * @return OrderModel
     */
    abstract public function setOrderStatus(OrderModel $orderModel);
}
