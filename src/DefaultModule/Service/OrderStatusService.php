<?php

namespace Src\DefaultModule\Service;

use Src\DefaultModule\Model\Order\DeliveredOrderStatus;
use Src\DefaultModule\Model\Order\InProgressOrderStatus;
use Src\DefaultModule\Model\Order\OrderModel;
use Src\DefaultModule\Model\Order\PendingOrderStatus;

/**
 * Class OrderStatusService
 */
class OrderStatusService
{
    /**
     * @var PendingOrderStatus
     */
    protected $pendingStatus;

    /**
     * @var InProgressOrderStatus;
     */
    protected $inProgressStatus;

    /**
     * @var DeliveredOrderStatus;
     */
    protected $deliveredStatus;

    /**
     * OrderStatusService constructor.
     *
     * @param PendingOrderStatus    $pendingStatus
     * @param InProgressOrderStatus $inProgressStatus
     * @param DeliveredOrderStatus  $deliveredStatus
     */
    public function __construct(PendingOrderStatus $pendingStatus, InProgressOrderStatus $inProgressStatus, DeliveredOrderStatus $deliveredStatus)
    {
        $this->pendingStatus = $pendingStatus;
        $this->inProgressStatus = $inProgressStatus;
        $this->deliveredStatus = $deliveredStatus;

        $this->pendingStatus->setNextStatus($this->inProgressStatus);
        $this->inProgressStatus->setNextStatus($this->deliveredStatus);
    }

    /**
     * Get next orderStatus.
     *
     * @param OrderModel $order
     *
     * @return OrderModel
     */
    public function getNextStatus($order)
    {
        return $this->pendingStatus->setOrderStatus($order);
    }
}
