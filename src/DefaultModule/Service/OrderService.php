<?php

namespace Src\DefaultModule\Service;

use Src\DefaultModule\Model\Order\OrderModel;

/**
 * Class OrderService
 */
class OrderService
{
    /**
     * Create Order.
     *
     * @param int $status
     *
     * @return OrderModel
     */
    public function createOrder($status)
    {
        $order = new OrderModel();

        if (null !== $status) {
             $order->setStatus($status);
        }

        return $order;
    }
}
