<?php

namespace Src\DefaultModule\Controller;

use Core\Controller\CoreController;
use DI\Annotation\Inject;
use Src\DefaultModule\Manager\OrderManager;
use Src\DefaultModule\Model\Order\OrderModel;

/**
 * Class DesignPatternController
 */
class DesignPatternController extends CoreController
{
    /**
     * @var OrderManager
     * @Inject()
     */
    protected $orderManager;

    /**
     * @param int $oldStatus
     */
    public function chainOfResponsibility(int $oldStatus)
    {
        /** @var OrderModel $order */
        $order = $this->orderManager->createOrder($oldStatus);

        $this->orderManager->setNextOrderStatus($order);

        $this->render(
            '@DefaultModule/DesignPattern/ChainOfResponsibility.html.twig',
            [
                'newStatus' => $order->getStatus(),
                'oldStatus' => $oldStatus,
            ]
        );
    }
}
