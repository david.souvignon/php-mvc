<?php

namespace Src\DefaultModule\Controller;

use DI\Annotation\Inject;
use Src\DefaultModule\Manager\UserManager;
use Core\Controller\CoreController;
use Src\DefaultModule\Model\UserModel;

/**
 * Class IndexController
 */
class IndexController extends CoreController
{
    /**
     * @Inject
     * @var UserManager
     */
    protected $userManager;

    /**
     * Index action
     */
    public function index()
    {
        /** @var UserModel $user */
        $user = $this->userManager->getUserById(1);

        $this->render('@DefaultModule/Default/index.html.twig', ['user' => $user]);
    }

    /**
     * User by id action.
     *
     * @param int $id
     */
    public function user(int $id)
    {
        /** @var UserModel $user */
        $user = $this->userManager->getUserById($id);

        $this->render('@DefaultModule/Default/index.html.twig', ['user' => $user]);
    }

    /**
     * Tutorials action.
     *
     * @param int    $id
     * @param string $name
     * @param string $offert
     */
    public function tuto(int $id, string $name, string $offert)
    {
        /** @var UserModel $user */
        $user = $this->userManager->getUserById($id);

        $this->render('@DefaultModule/Default/index.html.twig', ['user' => $user]);
    }
}
