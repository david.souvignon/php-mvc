<?php

namespace Src\DefaultModule;

use Src\DefaultModule\Controller\DesignPatternController;
use Src\DefaultModule\Controller\IndexController;
use Core\Controller\CoreController;
use Core\Module\ModuleInterface;
use Core\Routing\Route;

/**
 * Class DefaultModule
 */
class DefaultModule implements ModuleInterface
{
    /**
     * Get Routes.
     *
     * @return array
     */
    public function getRoutes()
    {
        return [
            [
                Route::ID => 'index',
                Route::ROUTE_NAME => '',
                Route::CONTROLLER => IndexController::class,
                Route::ACTION => 'index',
                Route::ARGS => [],
            ],
            [
                Route::ID => 'index.user',
                Route::ROUTE_NAME => 'user/:id',
                Route::CONTROLLER => IndexController::class,
                Route::ACTION => 'user',
                Route::ARGS => [
                    'id' => '[\d]+',
                ],
            ],
            [
                Route::ID => 'index.tuto',
                Route::ROUTE_NAME => 'home/tuto/:id/:name/:offert',
                Route::CONTROLLER => IndexController::class,
                Route::ACTION => 'tuto',
                Route::ARGS => [
                    'id' => '[a-z\-0-9]+',
                    'name' => '[a-z\-0-9]+',
                    'offert' => '[a-z\-0-9]+',
                ],
            ],
            [
                Route::ID => 'design.pattern.chain.of.responsibility',
                Route::ROUTE_NAME => 'design-pattern/chain-of-responsibility/:status',
                Route::CONTROLLER => DesignPatternController::class,
                Route::ACTION => 'chainOfResponsibility',
                Route::ARGS => [
                    'status' => '[\d]+',
                ],
            ],
        ];
    }

    /**
     * Get view path module root directory.
     *
     * @return array
     */
    public function getViewDirectory()
    {
        return [
            'path' => __DIR__.'/view',
            'namespace' => 'DefaultModule',
        ];
    }
}
