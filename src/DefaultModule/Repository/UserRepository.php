<?php

namespace Src\DefaultModule\Repository;

use Core\Repository\CoreRepository;
use Src\DefaultModule\Model\UserModel;

/**
 * Class UserRepository
 */
class UserRepository extends CoreRepository
{
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->dbEntityTable = 'user';
        $this->mappedClass = UserModel::class;
    }
}
