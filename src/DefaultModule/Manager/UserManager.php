<?php

namespace Src\DefaultModule\Manager;

use Src\DefaultModule\Dao\UserDao;
use Src\DefaultModule\Model\UserModel;

/**
 * Class UserManager
 */
class UserManager
{
    /**
     * @var UserDao
     */
    protected $dao;

    /**
     * UserManager constructor.
     *
     * @param UserDao $dao
     */
    public function __construct(UserDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * Get a user model by id.
     *
     * @param int $id
     *
     * @return UserModel|null
     */
    public function getUserById($id)
    {
        return $this->dao->findOneById($id);
    }
}
