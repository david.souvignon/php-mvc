<?php

namespace Src\DefaultModule\Manager;

use Src\DefaultModule\Model\Order\OrderModel;
use Src\DefaultModule\Service\OrderService;
use Src\DefaultModule\Service\OrderStatusService;

/**
 * Class OrderManager
 */
class OrderManager
{
    /**
     * @var OrderService
     */
    protected $orderService;

    /**
     * @var OrderStatusService
     */
    protected $orderStatusService;

    /**
     * OrderManager constructor.
     *
     * @param OrderService       $orderService
     * @param OrderStatusService $orderStatusService
     */
    public function __construct(OrderService $orderService, OrderStatusService $orderStatusService)
    {
        $this->orderService = $orderService;
        $this->orderStatusService = $orderStatusService;
    }

    /**
     * Create order.
     *
     * @param int $status
     *
     * @return OrderModel
     */
    public function createOrder($status)
    {
        return $this->orderService->createOrder($status);
    }

    /**
     * Set the next orderStatus.
     *
     * @param OrderModel $order
     *
     * @return OrderModel
     */
    public function setNextOrderStatus(OrderModel $order)
    {
        return $this->orderStatusService->getNextStatus($order);
    }
}
