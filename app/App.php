<?php

namespace App;

use Core\Controller\CoreController;
use Core\Database\DatabaseInterface;
use Core\Module\ModuleInterface;
use Core\Routing\Route;
use Core\Routing\RouterInterface;
use DI\Container;

/**
 * Class App
 */
class App
{
    /**
     * @var Container
     */
    protected static $container;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var DatabaseInterface
     */
    protected static $dbConnexion;

    /**
     * App constructor.
     *
     * @param RouterInterface   $router
     * @param DatabaseInterface $db
     */
    public function __construct(RouterInterface $router, DatabaseInterface $db)
    {
        $this->router = $router;
        self::$dbConnexion = $db;
    }

    /**
     * Set Dependency Injection Container.
     *
     * @param Container $container
     */
    public function setContainer(Container $container)
    {
        self::$container = $container;
    }

    /**
     * Get the container.
     *
     * @return Container
     */
    public static function getContainer()
    {
        return self::$container;
    }

    /**
     * Start serve page.
     *
     * @return callable
     *
     * @throws \Exception
     */
    public function start()
    {
        /** @var Route $route */
        $route = $this->router->matchRoute();

        if (null !== $route) {
            $classController = $route->getController();

            $controller = new $classController();
            $action = $route->getAction();
            $args = $route->getArgs();

            if (!empty($args) > 0) {
                return call_user_func_array(array($controller, $action), $args);
            }

            return call_user_func(array($controller, $action));
        }

        return call_user_func(array(new CoreController(), 'notFoundAction'));
    }

    /**
     * Get database connection.
     *
     * @return DatabaseInterface
     */
    public static function getDb()
    {
        return self::$dbConnexion;
    }

    /**
     * Register routes.
     *
     * @param array $modules
     */
    public function registerRoutes(array $modules)
    {
        foreach ($modules as $moduleClassName) {
            /** @var ModuleInterface $module */
            $module = new $moduleClassName();

            $this->router->registerRoutes($module->getRoutes());
            unset($module);
        }
    }
}
