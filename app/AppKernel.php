<?php

namespace App;

use DI\Container;
use DI\ContainerBuilder;

/**
 * Class AppKernel
 */
class AppKernel
{
    const CONFIG_PATH = __DIR__.'/config/config.php';

    /**
     * @var Container
     */
    protected $container;

    /**
     * AppKernel constructor.
     */
    public function __construct()
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->addDefinitions(self::CONFIG_PATH);
        $containerBuilder->useAnnotations(true);

        $this->container = $containerBuilder->build();

        $this->boot();
    }

    /**
     * Boot the App.
     */
    private function boot()
    {
        /** @var App $app */
        $app = $this->container->get('App');
        $app->setContainer($this->container);
        $app->start();
    }
}
