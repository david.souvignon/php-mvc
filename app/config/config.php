<?php

use App\App;
use Cocur\Slugify\Slugify;
use Core\Database\MySqlDatabase;
use Core\Extension\Twig\TwigExtensionDvidz;
use Core\Module\ModuleInterface;
use Core\Routing\RouteBuilder;
use DI\Container;
use function DI\get;
use function DI\object;

return [
    'database.driver' => 'mysql',
    'database.db_name' => 'shop',
    'database.db_user' => 'dvidz',
    'database.db_password' => 'keenon',
    'database.db_host' => 'localhost',

    'app.modules' => [
        'Core\\Module\\CoreModule',
        'Src\\DefaultModule\\DefaultModule',
    ],

    'MySqlDatabase' => function (Container $c) {
        return new MySqlDatabase(
            $c->get('database.db_host'),
            $c->get('database.db_name'),
            $c->get('database.db_user'),
            $c->get('database.db_password')
        );
    },

    'TwigEnv' => function () {
        $loader = new Twig_Loader_Filesystem();
        $cachePath = __DIR__.'/../cache/templates';

        return new Twig_Environment($loader, [
            'cache' => false,
            //'cache' => $cachePath,
        ]);
    },

    'RouteBuilder' => object(RouteBuilder::class),

    'Router' => object(Core\Routing\Router::class)
        ->method('setRouteBuilder', get('RouteBuilder')),

    'Slugify' => object(Slugify::class),

    'Twig' => function (Container $c) {
        $modules = $c->get('app.modules');
        $twig = $c->get('TwigEnv');
        $twig->addExtension(new TwigExtensionDvidz($c->get('Router'), $c->get('Slugify')));

        foreach ($modules as $moduleClassName) {
            /** @var ModuleInterface $module */
            $module = new $moduleClassName();

            $moduleViewDirectory = $module->getViewDirectory();
            $twig->getLoader()->addPath($moduleViewDirectory['path'], $moduleViewDirectory['namespace']);
            unset($module);
        }

        return $twig;
    },

    //Register all routes. Magic!
    'App' => object(App::class)
        ->constructor(
            get('Router'),
            get('MySqlDatabase')
        )
        ->method('registerRoutes', get('app.modules')),
];
