<?php

namespace Core\Repository;

/**
 * Class RepositoryFactory
 */
class RepositoryFactory implements RepositoryFactoryInterface
{
    /**
     * @var CoreRepositoryInterface[]
     */
    protected $repositories = [];

    /**
     * Return a CoreRepositoryInterface instance.
     *
     * @param string $className
     *
     * @return CoreRepositoryInterface
     */
    public function getRepository($className)
    {
        if (!isset($this->repositories[$className])) {
            $this->repositories[$className] = new $className();
        }

        return $this->repositories[$className];
    }
}
