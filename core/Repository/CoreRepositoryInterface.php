<?php

namespace Core\Repository;

/**
 * Interface CoreRepositoryInterface
 */
interface CoreRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return \stdClass|null
     */
    public function findOne($id);

    /**
     * @return mixed|null
     */
    public function findAll();
}
