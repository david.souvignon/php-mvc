<?php

namespace Core\Repository;

/**
 * Interface RepositoryFactoryInterface
 */
interface RepositoryFactoryInterface
{
    /**
     * @param string $className
     *
     * @return CoreRepositoryInterface
     */
    public function getRepository($className);
}
