<?php

namespace Core\Repository;

use App\App;
use PDO;

/**
 * Class CoreRepository
 */
class CoreRepository implements CoreRepositoryInterface
{
    /**
     * @var string|null
     */
    protected $dbEntityTable = null;

    /**
     * @var string|null
     */
    protected $mappedClass = null;

    /**
     * Find one by attributes.
     *
     * @param array $attr
     *
     * @return null|mixed
     */
    public function findOneBy(array $attr)
    {
        $sql = 'SELECT * FROM '.$this->dbEntityTable.' WHERE ';

        $i = 0;
        foreach ($attr as $key => $value) {
            if (!$i) {
                $sql .= $key = $value;
                $i++;
            } else {
                $sql .= ' AND '.$key = $value;
            }
        }

        $req = $this->getConnexion()->query($sql);
        $req->setFetchMode(PDO::FETCH_CLASS, $this->mappedClass);
        $data = $req->fetch();

        return $data;
    }

    /**
     * Find one.
     *
     * @param int $id
     *
     * @return null|mixed
     */
    public function findOne($id)
    {
        $sql = 'SELECT * FROM '.$this->dbEntityTable.' WHERE id = '.$id;
        $req = $this->getConnexion()->query($sql);
        $req->setFetchMode(PDO::FETCH_CLASS, $this->mappedClass);
        $data = $req->fetch();

        return $data;
    }

    /**
     * Find all.
     *
     * @return array
     */
    public function findAll()
    {
        $sql = 'SELECT * FROM '.$this->dbEntityTable;
        $req = $this->getConnexion()->query($sql);
        $req->setFetchMode(PDO::FETCH_CLASS, $this->mappedClass);
        $data = $req->fetchAll();

        return $data;
    }

    /**
     * @return PDO
     */
    private function getConnexion()
    {
        return App::getDb()->getConnexion();
    }
}
