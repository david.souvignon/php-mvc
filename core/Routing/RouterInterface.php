<?php

namespace Core\Routing;

/**
 * Interface RouterInterface
 */
interface RouterInterface
{
    /**
     * @param Route $route
     *
     * @return mixed
     */
    public function addRoute(Route $route);

    /**
     * @return mixed
     */
    public function getAllRoutes();

    /**
     * @param string $routeName
     *
     * @return mixed
     */
    public function getRoute(string $routeName);

    /**
     * @param array $routes
     */
    public function registerRoutes(array $routes);

    /**
     * @return mixed
     */
    public function matchRoute();

    /**
     * @param RouteBuilderInterface $builder
     */
    public function setRouteBuilder(RouteBuilderInterface $builder);
}
