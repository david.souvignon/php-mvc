<?php

namespace Core\Routing;

/**
 * Class RouteBuilder
 */
class RouteBuilder implements RouteBuilderInterface
{
    /**
     * @var Route
     */
    protected $route;

    /**
     * @param array $data
     *
     * @return Route
     */
    public function getRoute(array $data)
    {
        $this->route = new Route();

        $this->makeRouteId($data[Route::ID]);
        $this->makeAction($data[Route::ACTION]);
        $this->makeArgs($data[Route::ARGS]);
        $this->makeController($data[Route::CONTROLLER]);
        $this->makeRouteName($data[Route::ROUTE_NAME]);

        return $this->route;
    }

    /**
     * @param string $name
     *
     * @return RouteBuilder
     */
    public function makeRouteName(string $name)
    {
        $this->route->setRouteName($name);

        return $this;
    }

    /**
     * @param string $controller
     *
     * @return RouteBuilder
     */
    public function makeController(string $controller)
    {
        $this->route->setController($controller);

        return $this;
    }

    /**
     * @param string $action
     *
     * @return RouteBuilder
     */
    public function makeAction(string $action)
    {
        $this->route->setAction($action);

        return $this;
    }

    /**
     * @param array $args
     *
     * @return RouteBuilder
     */
    public function makeArgs(array $args)
    {
        $this->route->setArgs($args);

        return $this;
    }

    /**
     * @param string $id
     *
     * @return RouteBuilder
     */
    public function makeRouteId(string $id): RouteBuilder
    {
        $this->route->setId($id);

        return $this;
    }
}
