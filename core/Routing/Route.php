<?php

namespace Core\Routing;

/**
 * Class Route
 */
class Route implements RouteInterface
{
    const METHOD = 'method';
    const ROUTE_NAME = 'routeName';
    const CONTROLLER = 'controller';
    const ACTION = 'action';
    const ARGS = 'args';
    const ID = 'id';

    /**
     * @var string
     */
    protected $routeName;

    /**
     * @var string
     */
    protected $controller;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var array
     */
    protected $args;

    /**
     * @var string
     */
    protected $id;

    /**
     * Get the route name.
     *
     * @return string
     */
    public function getRouteName(): string
    {
        return $this->routeName;
    }

    /**
     * Set the route name.
     *
     * @param string $routeName
     *
     * @return Route
     */
    public function setRouteName(string $routeName): Route
    {
        $this->routeName = $routeName;

        return $this;
    }

    /**
     * Get the route controller.
     *
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * Set the route controller.
     *
     * @param string $controller
     *
     * @return Route
     */
    public function setController(string $controller): Route
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     *
     * @return Route
     */
    public function setAction(string $action): Route
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return array
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * @param array $args
     *
     * @return Route
     */
    public function setArgs(array $args): Route
    {
        $this->args = $args;

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Route
     */
    public function setId(string $id): Route
    {
        $this->id = $id;

        return $this;
    }
}
