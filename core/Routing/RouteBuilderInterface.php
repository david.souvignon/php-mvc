<?php

namespace Core\Routing;

/**
 * Interface RouteBuilderInterface
 */
interface RouteBuilderInterface
{
    /**
     * @param array $data
     *
     * @return mixed
     */
    public function getRoute(array $data);

    /**
     * @param string $id
     *
     * @return mixed
     */
    public function makeRouteId(string $id);

    /**
     * @param string $routeName
     *
     * @return mixed
     */
    public function makeRouteName(string $routeName);

    /**
     * @param string $controller
     *
     * @return mixed
     */
    public function makeController(string $controller);

    /**
     * @param string $action
     *
     * @return mixed
     */
    public function makeAction(string $action);

    /**
     * @param array $args
     *
     * @return mixed
     */
    public function makeArgs(array $args);
}
