<?php

namespace Core\Routing;

/**
 * Class Router
 */
class Router implements RouterInterface
{
     /**
      * @var array
      */
    protected $routes = [];

    /**
     * @var array
     */
    protected $requirements;

    /**
     * @var RouteBuilderInterface
     */
    protected $routeBuilder;

    /**
     * @param RouteBuilderInterface $builder
     */
    public function setRouteBuilder(RouteBuilderInterface $builder)
    {
        $this->routeBuilder = $builder;
    }

     /**
      * Add Route to the routes collection.
      *
      * @param Route $route
      *
      * @return mixed|void
      */
    public function addRoute(Route $route)
    {
        if (!isset($this->routes[$route->getId()])) {
            $this->routes[$route->getId()] = $route;
        }
    }

    /**
     * Get Route object by route name.
     *
     * @param string $routeId
     *
     * @return Route|null
     *
     * @throws \Exception
     *
     */
    public function getRoute(string $routeId)
    {
        if (isset($this->routes[$routeId])) {
            return $this->routes[$routeId];
        }

        throw new \Exception('No route matches');
    }

    /**
    * Get all routes.
    *
    * @return Route[]
    */
    public function getAllRoutes()
    {
        return $this->routes;
    }

    /**
    * @param Route[] $routes
    */
    public function registerRoutes(array $routes)
    {
        foreach ($routes as $route) {
            $this->addRoute($this->buildRoute($route));
        }
    }

    /**
     * Get first route matches against registered route.
     *
     * @return Route|null
     */
    public function matchRoute()
    {
        /** @var Route $route */
        foreach ($this->getAllRoutes() as $route) {
            $this->requirements = $route->getArgs();

            $path = preg_replace_callback('#:([\w]+)#', [$this, 'requirementsMatch'], $route->getRouteName());
            $url = trim($_SERVER['REQUEST_URI'], '/');
            $regex = '#^'.$path.'$#i';

            if (preg_match($regex, $url, $matches)) {
                array_shift($matches);
                $route->setArgs($matches);

                return $this->routes[$route->getId()];
            }
        }

        return null;
    }

    /**
    * Build Route object from an array.
    *
    * @param array $dataRoute
    *
    * @return Route
    */
    private function buildRoute(array $dataRoute)
    {
        return $this->routeBuilder->getRoute($dataRoute);
    }

    /**
     * Callback to match args.
     *
     * @param array $match
     *
     * @return string
     */
    private function requirementsMatch($match)
    {
        if (isset($this->requirements[$match[1]])) {
            return '('.$this->requirements[$match[1]].')';
        }

        return '([^/]+)';
    }
}
