<?php

namespace Core\Routing;

/**
 * Interface RouteInterface
 */
interface RouteInterface
{
    /**
     * @return string
     */
    public function getRouteName();
}
