<?php

namespace Core\Extension\Twig;

use Cocur\Slugify\Slugify;
use Core\Routing\RouteInterface;
use Core\Routing\RouterInterface;
use Twig_Extension;

/**
 * Class TwigExtensionDvidz
 */
class TwigExtensionDvidz extends Twig_Extension
{
    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var Slugify
     */
    protected $slugify;

    /**
     * Twig_Extension_dvidz constructor.
     *
     * @param RouterInterface $router
     * @param Slugify         $slugify
     */
    public function __construct(RouterInterface $router, Slugify $slugify)
    {
        $this->router = $router;
        $this->slugify = $slugify;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_Function('render', array($this, 'renderController')),
            new \Twig_Function('url', array($this, 'url'), array('is_safe' => array('html'))),
        );
    }

    /**
     * Call a controller action to render the associated view.
     *
     * @param string $routeName
     * @param array  $args
     *
     * @return callable
     */
    public function renderController($routeName, $args)
    {
        $controllerClass = $this->router->getRoute($routeName)->getController();
        $action = $this->router->getRoute($routeName)->getAction();

        $controller = new $controllerClass();

        return $controller->$action($args);
    }

    /**
     * @param string $routeId
     * @param array  $args
     *
     * @return string
     */
    public function url(string $routeId, array $args)
    {
        return '<a href="/'.$this->geturl($routeId, $args).'">Lien</a>';
    }

    /**
     * @param string $routeName
     * @param array  $args
     *
     * @return string
     */
    protected function geturl(string $routeName, array $args): string
    {
        /** @var RouteInterface $route */
        $route = $this->router->getRoute($routeName);

        $url = $route->getRouteName();

        if (null != $args) {
            foreach ($args as $param => $value) {
                $url = str_replace(':'.$param, $this->slugify->slugify($value), $url);
            }
        }

        return $url;
    }
}
