<?php

namespace Core\Database;

/**
 * Class MySqlDatabase
 */
class MySqlDatabase extends AbstractDatabase implements DatabaseInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConnexion()
    {
        return $this->connexion;
    }
}
