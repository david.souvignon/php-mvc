<?php

namespace Core\Database;

/**
 * Interface DatabaseInterface
 */
interface DatabaseInterface
{
    /**
     * @return \PDO
     */
    public function getConnexion();
}
