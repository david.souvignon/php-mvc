<?php

namespace Core\Database;

use PDO;

/**
 * Class AbstractDatabase
 */
abstract class AbstractDatabase
{
    /**
     * @var string
     */
    protected $dbHost;

    /**
     * @var string
     */
    protected $dbName;

    /**
     * @var string
     */
    protected $dbUser;

    /**
     * @var string
     */
    protected $dbPassword;

    /**
     * @var PDO
     */
    protected $connexion;

    /**
     * AbstractDatabase constructor.
     * @param string $dbHost
     * @param string $dbName
     * @param string $dbUser
     * @param string $dbPassword
     */
    public function __construct(string $dbHost, string $dbName, string $dbUser, string $dbPassword)
    {
        $this->dbHost = $dbHost;
        $this->dbName = $dbName;
        $this->dbUser = $dbUser;
        $this->dbPassword = $dbPassword;

        $this->connectToDb();
    }

    /**
     * Connect to database
     */
    protected function connectToDb()
    {
        try {
            $this->connexion = new \PDO('mysql:host='.$this->dbHost.';dbname='.$this->dbName, $this->dbUser, $this->dbPassword);
        } catch (\PDOException $e) {
            throw new \Exception('Can not connect to database: '.$e->getMessage());
        }
    }
}
