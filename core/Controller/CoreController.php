<?php

namespace Core\Controller;

use App\App;
use Core\Routing\RouterInterface;
use DI\Annotation\Inject;
use Twig_Environment;

/**
 * Class CoreController
 */
class CoreController
{
    /**
     * @Inject("Twig")
     *
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @Inject("Router")
     *
     * @var RouterInterface
     */
    protected $router;

    /**
     * CoreController constructor.
     */
    public function __construct()
    {
        App::getContainer()->injectOn($this);
    }

    /**
     * Not Found action.
     */
    public function notFoundAction()
    {
        http_response_code(404);
        $this->render('@CoreModule/404.html.twig');
    }

    /**
     * Render view.
     *
     * @param string $view
     * @param array  $data
     */
    protected function render(string $view, array $data = [])
    {
        echo $this->twig->render($view, $data);
    }
}
