<?php

namespace Core\Module;

/**
 * Class CoreModule
 */
class CoreModule implements ModuleInterface
{
    /**
     * Get routes.
     *
     * @return array
     */
    public function getRoutes()
    {
        return [];
    }

    /**
     * Get view directory.
     *
     * @return array
     */
    public function getViewDirectory()
    {
        return [
            'path' => __DIR__.'/view',
            'namespace' => 'CoreModule',
        ];
    }
}
