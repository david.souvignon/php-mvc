<?php

namespace Core\Module;

/**
 * Interface ModuleInterface
 */
interface ModuleInterface
{
    /**
     * Get all route for the current module.
     *
     * @return array
     */
    public function getRoutes();

    /**
     * Get view directory path.
     *
     * @return array
     */
    public function getViewDirectory();
}
